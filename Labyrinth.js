/// <reference path = "lib/phaser.d.ts"/>
/// <reference path = "states/Boot.ts"/>
/// <reference path = "states/Game.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Labyrinth;
(function (Labyrinth_1) {
    var Labyrinth = /** @class */ (function (_super) {
        __extends(Labyrinth, _super);
        function Labyrinth(width, height) {
            var _this = _super.call(this, width, height, Phaser.AUTO, 'maze-id') || this;
            _this.state.add("Boot", Labyrinth_1.Boot, false);
            _this.state.add("Game", Labyrinth_1.Game, false);
            _this.state.start("Boot");
            return _this;
        }
        return Labyrinth;
    }(Phaser.Game));
    window.onload = function () {
        new Labyrinth(256, 256);
    };
})(Labyrinth || (Labyrinth = {}));
