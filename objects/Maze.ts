/// <reference path = "../lib/phaser.d.ts"/>

module Labyrinth {
    export class Maze extends Phaser.Tilemap {
        private cols: number = 8;
        private rows: number = 8;
        private tileSize: number = 32;
        private tilesMaze;
        private bunny: Phaser.Sprite;
        constructor(game: Phaser.Game) {
            super(game);
            this.bunny = game.add.sprite(0, 0, 'bunny');

            // 1 = wall, 0 = path, 2 = exit, 3 = bunny
            this.tilesMaze =  [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 1, 1, 1, 1, 0],
                [0, 1, 0, 1, 0, 2, 1, 0],
                [0, 1, 0, 1, 0, 1, 1, 0],
                [0, 1, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 1, 0, 0, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0]
            ];
        }

        makeMap() {
            for (let r = 0; r < this.rows * this.tileSize; r+=32) {
                for (let c = 0; c < this.cols * this.tileSize; c+=32) {
                    if( this.tilesMaze[c/32][r/32] === 1) {
                        this.game.add.tileSprite(r, c, 32, 32, 'tree');
                    }

                    if( this.tilesMaze[c/32][r/32] === 2) {
                        this.game.add.tileSprite(r, c, 32, 32, 'coin');
                    }
                }
            }
        }

        private drawMarked(row: number , col: number) {
            this.bunny.position.x = row*32;
            this.bunny.position.y = col*32;
        }

        public pathFinding(x: number, y: number, distance: number) {
            if (x < 0 || x > (this.tilesMaze[0].length -1) ||
                y < 0 || y > (this.tilesMaze.length - 1))  {
                return false;
            }

            if (this.tilesMaze[y][x] === 1 || this.tilesMaze[y][x] === 9 ) {
                return false;
            }

            if (this.tilesMaze[y][x] === 2) {
                console.log('Coin: ' + x + ':' + y + " distance: " + distance) ;
                return true;
            }
            //this.drawMarked(x, y);
            console.log('Position: ' + x + ':' + y);

            this.tilesMaze[y][x] = 9; //marked for visited

            this.pathFinding(x - 1, y, distance + 1); //up
            this.pathFinding(x, y + 1, distance + 1); //right
            this.pathFinding(x + 1, y, distance + 1); //down
            this.pathFinding(x, y - 1, distance + 1); //left

            this.tilesMaze[y][x] = 0; //unmarked
        }
    }
}