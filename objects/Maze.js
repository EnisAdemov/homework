/// <reference path = "../lib/phaser.d.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Labyrinth;
(function (Labyrinth) {
    var Maze = /** @class */ (function (_super) {
        __extends(Maze, _super);
        function Maze(game) {
            var _this = _super.call(this, game) || this;
            _this.cols = 8;
            _this.rows = 8;
            _this.tileSize = 32;
            _this.bunny = game.add.sprite(0, 0, 'bunny');
            // 1 = wall, 0 = path, 2 = exit, 3 = bunny
            _this.tilesMaze = [
                [0, 0, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 1, 1, 1, 1, 0],
                [0, 1, 0, 1, 0, 2, 1, 0],
                [0, 1, 0, 1, 0, 1, 1, 0],
                [0, 1, 0, 0, 0, 0, 0, 0],
                [0, 1, 0, 1, 0, 0, 1, 0],
                [0, 1, 1, 1, 1, 1, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0]
            ];
            return _this;
        }
        Maze.prototype.makeMap = function () {
            for (var r = 0; r < this.rows * this.tileSize; r += 32) {
                for (var c = 0; c < this.cols * this.tileSize; c += 32) {
                    if (this.tilesMaze[c / 32][r / 32] === 1) {
                        this.game.add.tileSprite(r, c, 32, 32, 'tree');
                    }
                    if (this.tilesMaze[c / 32][r / 32] === 2) {
                        this.game.add.tileSprite(r, c, 32, 32, 'coin');
                    }
                }
            }
        };
        Maze.prototype.drawMarked = function (row, col) {
            this.bunny.position.x = row * 32;
            this.bunny.position.y = col * 32;
        };
        Maze.prototype.pathFinding = function (x, y, distance) {
            if (x < 0 || x > (this.tilesMaze[0].length - 1) ||
                y < 0 || y > (this.tilesMaze.length - 1)) {
                return false;
            }
            if (this.tilesMaze[y][x] === 1 || this.tilesMaze[y][x] === 9) {
                return false;
            }
            if (this.tilesMaze[y][x] === 2) {
                console.log('Coin: ' + x + ':' + y + " distance: " + distance);
                return true;
            }
            //this.drawMarked(x, y);
            console.log('Position: ' + x + ':' + y);
            this.tilesMaze[y][x] = 9; //marked for visited
            this.pathFinding(x - 1, y, distance + 1); //up
            this.pathFinding(x, y + 1, distance + 1); //right
            this.pathFinding(x + 1, y, distance + 1); //down
            this.pathFinding(x, y - 1, distance + 1); //left
            this.tilesMaze[y][x] = 0; //unmarked
        };
        return Maze;
    }(Phaser.Tilemap));
    Labyrinth.Maze = Maze;
})(Labyrinth || (Labyrinth = {}));
