/// <reference path = "../lib/phaser.d.ts"/>

module Labyrinth {
    export class Boot extends Phaser.State {

        preload() {
            this.game.load.image('bunny', "assets/bunny.png");
            this.game.load.image('tree', "assets/tree.png");
            this.game.load.image('coin', "assets/coin.png");
        }

        create() {
            this.game.state.start("Game");
        }

    }
}