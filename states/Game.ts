/// <reference path = "../lib/phaser.d.ts"/>
/// <reference path = "../objects/Maze.ts"/>

module Labyrinth {
    export class Game extends Phaser.State {
        private maze: Maze;

        create() {
            this.maze = new Maze(this.game);

            this.maze.makeMap();
            this.maze.pathFinding(0, 0, 0);
        }

        update() {

        }

    }
}