/// <reference path = "../lib/phaser.d.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Labyrinth;
(function (Labyrinth) {
    var Boot = /** @class */ (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Boot.prototype.preload = function () {
            this.game.load.image('bunny', "assets/bunny.png");
            this.game.load.image('tree', "assets/tree.png");
            this.game.load.image('coin', "assets/coin.png");
        };
        Boot.prototype.create = function () {
            this.game.state.start("Game");
        };
        return Boot;
    }(Phaser.State));
    Labyrinth.Boot = Boot;
})(Labyrinth || (Labyrinth = {}));
