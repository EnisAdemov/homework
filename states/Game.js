/// <reference path = "../lib/phaser.d.ts"/>
/// <reference path = "../objects/Maze.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Labyrinth;
(function (Labyrinth) {
    var Game = /** @class */ (function (_super) {
        __extends(Game, _super);
        function Game() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Game.prototype.create = function () {
            this.maze = new Labyrinth.Maze(this.game);
            this.maze.makeMap();
            this.maze.pathFinding(0, 0, 0);
        };
        Game.prototype.update = function () {
        };
        return Game;
    }(Phaser.State));
    Labyrinth.Game = Game;
})(Labyrinth || (Labyrinth = {}));
