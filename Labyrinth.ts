/// <reference path = "lib/phaser.d.ts"/>
/// <reference path = "states/Boot.ts"/>
/// <reference path = "states/Game.ts"/>

module Labyrinth {
    class Labyrinth extends Phaser.Game {
        game:Phaser.Game;

        constructor(width?:number, height?:number) {
            super(width, height, Phaser.AUTO, 'maze-id');
            this.state.add("Boot", Boot, false);
            this.state.add("Game", Game, false);

            this.state.start("Boot");
        }
    }

    window.onload = ()=> {
        new Labyrinth(256, 256);
    }
}